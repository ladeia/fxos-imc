App simples criado com o intuito de testar o processo de lançar um app no marketplace da Mozilla Firefox OS.

Este App faz o cálculo do indice de massa corpórea (IMC) com base na tabela da Organização Mundial de Saúde (OMS). 
O IMC é um índice internacional de medir obesidade e sobrepeso, que por sua vez podem ser indicadores ipertensão arterial, a doença arterial coronariana e o diabetes melittus, além de outras patologias consideradas de alto risco para a Saúde Pública.

App criado por 
Antonio Ladeia - programador
Luiz HenRick - designer(ícones)

Este app está licensiado sob a Mozilla Public License, version 2.0.

