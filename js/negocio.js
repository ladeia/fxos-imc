var botaoCalculo = document.getElementById('calcular');
botaoCalculo.addEventListener("click", calcularIMC, false);

function calcularIMC(){
    var peso = document.getElementById('peso').value;
    var altura = document.getElementById('altura').value;
    var resultado = document.getElementById('resultado');
                
    //tratar numeros com ,
    altura = altura.replace(",",".");
                
    var imc = peso / (altura * altura);
    imc = imc.toFixed(2);
                
    var mensagem = "";

    if(imc < 17)
 		mensagem = "Muito abaixo do peso.";

	if(imc >= 17 && imc < 18.5)
		mensagem = "Abaixo do peso.";

    if(imc >= 18.5 && imc < 25)
        mensagem = "Peso normal.";
        		
    if(imc >= 25 && imc <  30)
    	mensagem = "Sobrepeso.";
        		
    if(imc >= 30 && imc < 35 )
		mensagem = "Obesidade grau I.";
        	
	if(imc >= 35 && imc < 40)
    	mensagem = "Obesidade grau II (severa).";
        		
    if(imc >= 40)
    	mensagem = "Obesidade grau III (mórbida).";
                
    resultado.innerHTML = imc+"<br />"+mensagem;
} 
